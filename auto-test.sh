#!/bin/bash
set -ex

SCRIPT_PATH="$(readlink -f "$0")"
SCRIPT_DIR="$(dirname "$SCRIPT_PATH")"

WORK_DIR=~/auto-build
cd "$WORK_DIR/state/building"
shopt -s nullglob

while sleep 10; do
	tasks=(*)

	[ ${#tasks[@]} -eq 0 ] && continue

	# 52926164 build (rhel-9.3.0-test-pesign, kernel-6.4.0_rc3-tnguy.next.dq.367c72ccb915.el9.src.rpm) completed successfully
	# 52926147 build (rhel-9.3.0-test-pesign, kernel-6.4.0_rc3-tnguy.next.dq.367c72ccb915.el9.src.rpm) was canceled
	# 52495852 build (rhel-9.3.0-test-pesign, kernel-6.3.0-tnguy.net.dq.1ff1102d4d1b.net_driver_ice.el9.src.rpm) failed
	brew watch-task ${tasks[0]} | while read line; do
		regex='^([0-9]+) build \(.*\) ([ a-z]+)$'
		[[ "$line" =~ $regex ]] || continue

		taskid=${BASH_REMATCH[1]}
		state=${BASH_REMATCH[2]}
		case $state in
			"completed successfully")
				config_name="$(cat "$taskid")"

				drivers=
				source "$WORK_DIR/defaults.conf"
				source "$WORK_DIR/configs/$config_name.conf"

				queue_item="$($SCRIPT_DIR/lnst-jenkins-trigger.sh "$taskid" "$drivers")"
				mv "$taskid" "../testing/$queue_item"
				;;
			"failed"|"was canceled")
				mv "$taskid" ../failed/
				;;
			*)
				echo "$taskid unexpected: $state"
				exit 1
				;;
		esac
	done
done
