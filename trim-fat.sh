#!/bin/sh
set -ex

SCRIPT_PATH="$(readlink -f "$0")"
SCRIPT_DIR="$(dirname "$SCRIPT_PATH")"

## Trim some fat
## Reduced debuginfo makes the build much smaller and is sufficient for our purpose
#sed -i -e 's/# CONFIG_DEBUG_INFO_REDUCED is not set/CONFIG_DEBUG_INFO_REDUCED=y/' .config
# Just disable debuginfo altogether. olddefconfig will default to CONFIG_DEBUG_INFO_NONE=y.
sed -i -e 's/^CONFIG_DEBUG_INFO.*//' .config
# Remove subsystems we don't need. Even from the source. It makes the src.rpm smaller.
# DRM
sed -i -e 's/CONFIG_DRM=m/# CONFIG_DRM is not set/' .config
sed -i -e 's#source "drivers/gpu/drm/Kconfig"##' drivers/video/Kconfig
sed -i -e 's# drm/##' drivers/gpu/Makefile
git rm -r drivers/gpu/drm/
# FB is not that big, but depends on something from drm.
sed -i -e 's/CONFIG_FB=y/# CONFIG_FB is not set/' .config
# media
sed -i -e 's/CONFIG_MEDIA_SUPPORT=m/# CONFIG_MEDIA_SUPPORT is not set/' .config
sed -i -e 's#source "drivers/media/Kconfig"##' drivers/Kconfig
sed -i -e 's# media/##' drivers/Makefile
git rm -r drivers/media/
# sound
sed -i -e 's/CONFIG_SOUND=m/# CONFIG_SOUND is not set/' .config
sed -i -e 's#source "sound/Kconfig"##' drivers/Kconfig
sed -i -e 's# sound/##' Kbuild
git rm -r sound/
# HID
sed -i -e 's/CONFIG_HID_SUPPORT=y/# CONFIG_HID_SUPPORT is not set/' .config
# wireless
sed -i -e 's/CONFIG_WLAN=y/# CONFIG_WLAN is not set/' .config
sed -i -e 's/CONFIG_WIRELESS=y/# CONFIG_WIRELESS is not set/' .config
sed -i -e 's#source "drivers/net/wireless/Kconfig"##' drivers/net/Kconfig
git rm -r drivers/net/wireless/
# Non-x86 archs. Leave Kconfigs and Makefiles around, because some are referenced from elsewhere.
find arch/ \( -name x86 -o -name Kconfig\* -o -name Makefile\* \) -prune -o \( -type f -print0 \) | xargs -0 git rm

git commit -a -m 'trim fat'
