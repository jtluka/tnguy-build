#!/bin/bash
set -ex

SCRIPT_PATH="$(readlink -f "$0")"
SCRIPT_DIR="$(dirname "$SCRIPT_PATH")"

WORK_DIR=~/auto-build
cd "$WORK_DIR"

NPROC=$(nproc || echo 4)

for conf in configs/*.conf; do
	gittagprefix=
	remote=
	branch=
	config=
	disttag=
	postprocess=
	suffix=
	target=
	drivers=
	source ./defaults.conf
	source "$conf"

	config_name=$(basename "$conf" .conf)

	cd linux
	git fetch "$remote"

	ref=$(git rev-parse --short "$remote/$branch")
	old_ref=$(cat "$WORK_DIR/state/heads/$config_name.head" || echo none)

	if [ "$ref" = "$old_ref" ]; then
		echo "Nothing new for $config_name ($remote/$branch)."
		cd ..
		continue
	fi

	git clean -fdx
	git checkout -f "$remote/$branch"

	# Use the given kernel config file as the starting point
	cp "$WORK_DIR/$config" .config
	git add -f .config; git commit -m 'add base config'
	make -j"$NPROC" olddefconfig
	git add -f .config; git commit --allow-empty -m 'after olddefconfig'

	"$SCRIPT_DIR/$postprocess"
	git add -f .config; git commit --allow-empty -m 'after postprocessing'
	make -j"$NPROC" olddefconfig
	git add -f .config; git commit --allow-empty -m 'after 2nd olddefconfig'

	verstring="$suffix.$ref$disttag"
	echo "$verstring" > .version
	# Replace the 'build-version' script that auto-increments .version with a dummy
	echo -e '#!/bin/sh\ncat .version' > init/build-version

	srcrpm=$(make -j"$NPROC" srcrpm-pkg LOCALVERSION="" KGZIP="zstd -T0 -10" | awk '/^Wrote:/ {print $2;}')
	taskid=$(brew build --nowait --noprogress --scratch --arch-override=x86_64 "$target" "$srcrpm" | awk '/^Created task:/ {print $3;}')
	[[ "$taskid" =~ ^[0-9]+$ ]]
	echo "$config_name" > "$WORK_DIR/state/tmp/$taskid"
	mv "$WORK_DIR/state/tmp/$taskid" "$WORK_DIR/state/building/$taskid"
	git add -f .version init/build-version; git commit -m "Brew task ID $taskid"

	git clean -fdx
	git tag -f "$gittagprefix$verstring"
	echo "$ref" > "$WORK_DIR/state/heads/$config_name.head"
	cd ..
done
